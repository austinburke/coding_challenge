const request = require('request-promise-native');

const url = 'https://www.healthcare.gov/api/glossary.json';

module.exports.handler = async (language) => {
  const response = await request.get(url);
  const data = JSON.parse(response);
  const filt = data.glossary.filter(element => element.lang === language);
  const langArray = [];

  // if the filtered array's length is equal to zero, then return all aticles
  if (filt.length === 0) {
    return data.glossary;
  }
  // Adds the filtered articles to the array
  filt.forEach((element) => {
    langArray.push(element);
    console.log(langArray);
  });
};

// Passes in the desired language of the article
this.handler('en');
